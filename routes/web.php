<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function() {
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::auth();
	Route::get('/home', 'HomeController@index');
	
	// contact
	Route::get('contact', 'ContactController@index');
	Route::get('contact/create' , 'ContactController@create');
	Route::post('contact/store' , 'ContactController@store');
	Route::get('contact/edit/{edit}', 'ContactController@edit');
	Route::get('contact/show/{edit}', 'ContactController@show');
	Route::put('contact/update/{edit}', 'ContactController@update');
	Route::delete('contact/delete/{del}', 'ContactController@destroy');
	
	Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin']], function() {
		// users
		Route::get('user', 'UserController@index');
		Route::get('user/create', 'UserController@create');
		Route::post('user/store', 'UserController@store');
		Route::get('user/edit/{id}', 'UserController@edit');
		Route::put('user/update/{id}', 'UserController@update');
		Route::delete('user/delete/{id}', 'UserController@destroy');

		// roles
		Route::get('roles', 'RoleController@index');
		Route::get('roles/create', 'RoleController@create');
		Route::get('roles/edit/{id}', 'RoleController@edit');
		Route::post('roles/store', 'RoleController@store');
		Route::put('roles/update/{id}', 'RoleController@update');
		Route::delete('roles/delete/{id}', 'RoleController@destroy');

		// permission
		Route::get('permission', 'PermissionController@index');
		Route::get('permission/create', 'PermissionController@create');
		Route::get('permission/edit/{id}', 'PermissionController@edit');
		Route::post('permission/store', 'PermissionController@store');
		Route::put('permission/update/{id}', 'PermissionController@update');
		Route::delete('permission/delete/{id}', 'PermissionController@destroy');
	});
});
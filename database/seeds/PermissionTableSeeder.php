<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permission = [
        [
			'name'         => 'permAddContact',
			'display_name' => 'Permission Add Contact',
			'description'  => 'Akses untuk menambah contact'
	    ],
        [
			'name'         => 'permEditContact',
			'display_name' => 'Permission Edit Contact',
			'description'  => 'Akses untuk mengedit contact'
        ],
        [
			'name'         => 'permDelContact',
			'display_name' => 'Permission Delete Contact',
			'description'  => 'Akses Hapus Contact'
        ],
        [
			'name'         => 'permShowContact',
			'display_name' => 'Permission Melihat List Contact',
			'description'  => 'Akses hanya dapat melihat contact'
        ]];

        foreach ($permission as $key => $value)
        {
        	Permission::create($value);
        }
    }
}

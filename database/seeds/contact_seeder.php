<?php

use Illuminate\Database\Seeder;

class contact_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker   = Faker\Factory::create();
        $panjang = 200;
        
        for($i = 1; $i <= $panjang; $i++)
        {
        	DB::table('contacts')->insert([
        		'name'      => $faker->name, 
        		'alamat'    => $faker->address, 
        		'pekerjaan' => $faker->company, 
        		'no_telp'   => $faker->phoneNumber
        	]);
        }
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class users_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adminRole = new Role();
        $adminRole->name = "admin";
        $adminRole->display_name = "Admin";
        $adminRole->save();

        $memberRole = new Role();
        $memberRole->name = "member";
        $memberRole->display_name = "Member";
        $memberRole->save();

        $admin = new User();
        $admin->name     = "Admin Kulwap";
        $admin->email    = "admin@kulwap.com";
        $admin->password = bcrypt("kulwap");
        $admin->save();
        $admin->attachRole($adminRole);

        $member = new User();
        $member->name     = "Member Kulwap";
        $member->email    = "member@kulwap.com";
        $member->password = bcrypt("kulwap");
        $member->save();
        $admin->attachRole($memberRole);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = User::orderBy('id', 'ASC')->paginate(5);
        return view('users.index', compact('data'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::pluck('name', 'id');
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles'    => 'required'
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user  = User::create($input);
        $roles = $request->input('roles');

        foreach ($roles as $key => $value) 
        {
            $user->attachRole($value);
        }

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'User created successfully'
        ]);

        return redirect('admin/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $id)
    {
        //
        $user = $user::find($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user, Role $role, $id)
    {
        //
        $users    = $user::find($id);
        $roles    = $role::pluck('display_name', 'id');
        $userRole = $users->roles->pluck('id', 'id');

        return view('users.edit', compact('users', 'roles', 'userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, $id)
    {
        //
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles'    => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password']))
        {
            $input['password'] = Hash::make($input['password']);
        }
        else
        {
            $input = array_except($input, array('password'));    
        }

        $users = $user::find($id);
        $users->update($input);
        DB::table('role_user')->where('user_id', $id)->delete();

        $roles = $request->input('roles');
        foreach ($roles as $key => $value) 
        {
            $users->attachRole($value);
        }

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'User edited successfully'
        ]);

        return redirect('admin/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, $id)
    {
        //
        $user::find($id)->delete();
        
        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'User deleted successfully'
        ]);

        return redirect('admin/user');
    }
}

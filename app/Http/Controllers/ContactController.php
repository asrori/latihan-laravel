<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Session;

class ContactController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $builder)
    {
        //
        // $contact = Contact::paginate(10);
        // return view('contact.index', compact('contact'));
        // 
        if($request->ajax())
        {
            $contact = Contact::all();
            // return Datatables::of($contact)->make(true);
            return Datatables::of($contact)
                    ->addColumn('action', function($contact) {
                        return view('datatable._action', [
                            'edit_url' => url('/contact/edit', $contact->id),
                            'show_url' => url('/contact/show', $contact->id),
                            'id_del'   => $contact->id,
                        ]);
                    })->make(true);
        }
        
        $html = $builder
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Nama'])
            ->addColumn(['data' => 'alamat', 'name' => 'alamat', 'title' => 'Alamat'])
            ->addColumn(['data' => 'pekerjaan', 'name' => 'pekerjaan', 'title' => 'Pekerjaan'])
            ->addColumn(['data' => 'no_telp', 'name' => 'no_telp', 'title' => 'No Telp', 'orderable' => false, 'searchable' => false])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => '', 'orderable' => false, 'searchable' => false ]);

        return view('contact.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('contact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'      => 'required',
            'alamat'    => 'required',
            'pekerjaan' => 'required',
            'no_telp'   => 'required',
        ]);

        // $data = array('name' => $req->get('name'), 'address' => $req->get('alamat'))
        $data = $request->all();
        $contact = Contact::create($data);

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Berhasil menyimpan '.$contact->name
        ]);

        return redirect('contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contacts, $id)
    {
        //
        $contact = $contacts::find($id);

        return view('contact.show', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact, $id)
    {
        //
        // $contact = Contact::findOrFail($id);
        $contact = Contact::find($id);

        return view('contact.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact, $id)
    {
        //
        $contact = Contact::find($id);

        $this->validate($request, [
            'name'      => 'required',
            'alamat'    => 'required',
            'pekerjaan' => 'required',
            'no_telp'   => 'required',
        ]);

        // $data = array('name' => $req->get('name'), 'address' => $req->get('alamat'))
        $data = $request->all();
        $contact->update($data);

        return redirect('contact');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact, $id)
    {
        //
        // $contact = Contact::find($id);
        $contact = Contact::findOrFail($id);
        $contact->delete();
        
        return redirect('/contact');
    }
}

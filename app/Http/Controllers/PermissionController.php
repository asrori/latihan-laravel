<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use Session;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $permissions = Permission::orderBy('id', 'ASC')->paginate(10);
        return view('permissions.index', compact('permissions'))->with('i', ($request->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'         => 'required|unique:permissions,name',
            'display_name' => 'required',
            'description'  => 'required',
        ]);

        $permission               = new Permission();
        $permission->name         = $request->input('name');
        $permission->display_name = $request->input('display_name');
        $permission->description  = $request->input('description');
        $permission->save();

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Permissions created successfully'
        ]);

        return redirect('admin/permission');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permissions, $id)
    {
        //
        $permission = $permissions::find($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permissions, $id)
    {
        //
        $this->validate($request, [
            'display_name' => 'required',
            'description'  => 'required',
        ]);

        $permission               = $permissions::find($id);
        $permission->display_name = $request->input('display_name');
        $permission->description  = $request->input('description');
        $permission->save();

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Permissions updated successfully'
        ]);

        return redirect('admin/permission');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission, $id)
    {
        //
        $permission::where('id', $id)->delete();
        
         Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Permissions deleted successfully'
        ]);

        return redirect('admin/permission');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use DB;
use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $roles = Role::orderBy('id', 'DESC')->paginate(5);
        return view('roles.index', compact('roles'))->with('i', ($request->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $permission      = Permission::get();
        $rolePermissions = array();
        return view('roles.create', compact('permission', 'rolePermissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'         => 'required|unique:roles,name',
            'display_name' => 'required',
            'description'  => 'required',
            'permission'   => 'required',
        ]);

        $role               = new Role();
        $role->name         = $request->input('name');
        $role->display_name = $request->input('display_name');
        $role->description  = $request->input('description');
        $role->save();

        $permission = $request->input('permission');
        foreach ($permission as $key => $value) 
        {
            $role->attachPermission($value);
        }

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Roles created successfully'
        ]);

        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role, $id)
    {
        //
       /* $role = $role::find($id);
        $rolePermissions = Permission::join("permission_role", "permission_role.permission_id", "=", "permissions.id")->where("permission_role.role_id", $id)->get();

        return view('roles.show', compact('role', 'rolePermissions'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role, $id)
    {
        //
        $role            = $role::find($id);
        $permission      = Permission::get();
        $rolePermissions = DB::table("permission_role")->where("permission_role.role_id", $id)->pluck('permission_role.permission_id', 'permission_role.permission_id')->toArray();

        return view('roles.edit', compact('role', 'permission', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $roles, $id)
    {
        //
        $this->validate($request, [
            'display_name' => 'required',
            'description'  => 'required',
            'permission'   => 'required',
        ]);

        $role               = $roles::find($id);
        $role->display_name = $request->input('display_name');
        $role->description  = $request->input('description');
        $role->save();

        DB::table("permission_role")->where("permission_role.role_id", $id)->delete();

        $permission = $request->input('permission');
        foreach ($permission as $key => $value) 
        {
            $role->attachPermission($value);
        }

        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Roles edited successfully'
        ]);

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, $id)
    {
        //
        DB::table("roles")->where('id', $id)->delete();
        
        Session::flash('flash_notification', [
            'level'   => 'success',
            'message' => 'Roles deleted successfully'
        ]);

        return redirect('admin/roles');
    }
}

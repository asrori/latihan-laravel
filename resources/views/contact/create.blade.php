@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/contact') }}">Contact</a></li>
					<li class="active">Create Contact</a></li>
				</ul>
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Craete Contact</h2>
					</div>
					<div class="panel-body">
						{!! Form::open(['url' => '/contact/store', 'class' => 'form-horizontal']) !!}
							@include('contact._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection('content')
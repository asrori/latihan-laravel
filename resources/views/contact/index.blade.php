@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="active">Contact</li>
                </ul>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Data Contact</h3>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            @permission('permAddContact')
                                <p><a href="{{ url('/contact/create') }}" class="btn btn-default">Tambah</a></p>
                            @endpermission
                            {!! $html->table(['class' => 'table-striped table-bordered table-hover'])!!}
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {!! $html->scripts() !!}
@endsection
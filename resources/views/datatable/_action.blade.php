@permission('permEditContact')
<a href="{{ $edit_url }}" class="btn btn-xs btn-primary">Edit</a>
@endpermission

@permission('permDelContact')
	{!! Form::open(['method' => 'DELETE', 'url' => ['/contact/delete', $id_del], 'style' => 'display:inline', 'onclick' => 'return confirm("Anda yakin akan menghapus data ?")']) !!}
	{!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
	{!! Form::close() !!}
@endpermission

@permission('permShowContact')
<a href="{{ $show_url }}" class="btn btn-xs btn-success">Show</a>
@endpermission
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="active">User</li>
                </ul>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Data Users</h3>
                    </div>

                    <div class="panel-body">
                        @role('admin')
                            <p><a href="{{ url('/admin/user/create') }}" class="btn btn-primary">Tambah User</a></p>
                        @endrole
                        
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Roles</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $value)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $value->name }}</td>
                                        <td>{{ $value->email }}</td>
                                        <td>
                                            @if(!empty($value->roles))
                                                @foreach($value->roles as $v)
                                                    <label class="label label-success">{{ $v->display_name }}</label>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('admin/user/edit', $value->id) }}">Edit</a>
                                            {!! Form::open(['method' => 'DELETE', 'url' => ['admin/user/delete', $value->id], 'style' => 'display:inline', 'onclick' => 'return confirm("Anda yakin akan menghapus data ?")']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $data->render() !!}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
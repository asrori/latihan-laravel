	<div class="form-group" {{ $errors->has('name') ? 'has-errors' : '' }}>
		<label for="name" class="col-md-2 control-label">Name</label>
		<div class="col-md-4">
			{!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('email') ? 'has-errors' : '' }}>
		<label for="email" class="col-md-2 control-label">Email</label>
		<div class="col-md-4">
			{!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('password') ? 'has-errors' : '' }}>
		<label for="password" class="col-md-2 control-label">Password</label>
		<div class="col-md-4">
			 {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('confirm') ? 'has-errors' : '' }}>
		<label for="confirm" class="col-md-2 control-label">Confirm Password</label>
		<div class="col-md-4">
			{!! Form::password('confirm-password', ['placeholder' => 'Confirm Password', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('role') ? 'has-errors' : '' }}>
		<label for="role" class="col-md-2 control-label">Roles</label>
		<div class="col-md-4">
			 {!! Form::select('roles[]', $roles, null, ['class' => 'form-control', 'multiple']) !!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-md-offset-2">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
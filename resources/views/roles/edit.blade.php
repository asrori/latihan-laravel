@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/roles') }}">Roles</a></li>
					<li class="active">Edit Roles</a></li>
				</ul>
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Edit Role</h2>
					</div>
					<div class="panel-body">
						{!! Form::model($role, ['method' => 'PUT', 'url' => ['admin/roles/update', $role->id], 'class' => 'form-horizontal']) !!}
							@include('roles._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection('content')
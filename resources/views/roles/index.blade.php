@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="active">User</li>
                </ul>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Role Management</h3>
                    </div>

                    <div class="panel-body">
                        @role('admin')
                        <p><a class="btn btn-success" href="{{ url('/admin/roles/create') }}"> Tambah Role</a></p>
                        @endrole
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Role Name</th>
                                        <th>Description</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $key => $role)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->description }}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('admin/roles/edit',$role->id) }}">Edit</a>
                                        
                                            {!! Form::open(['method' => 'DELETE', 'url' => ['admin/roles/delete', $role->id], 'style' => 'display:inline', 'onclick' => 'return confirm("Anda yakin akan menghapus data ?")']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $roles->render() !!}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
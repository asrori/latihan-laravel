	<div class="form-group" {{ $errors->has('name') ? 'has-errors' : '' }}>
		<label for="name" class="col-md-2 control-label">Name Roles</label>
		<div class="col-md-4">
			{!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('display') ? 'has-errors' : '' }}>
		<label for="display" class="col-md-2 control-label">Display Name</label>
		<div class="col-md-4">
			{!! Form::text('display_name', null, ['placeholder' => 'Display Name', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('description') ? 'has-errors' : '' }}>
		<label for="description" class="col-md-2 control-label">Description</label>
		<div class="col-md-4">
			 {!! Form::textarea('description', null, ['placeholder' => 'Description', 'class' => 'form-control', 'style' => 'height:100px']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('Permission') ? 'has-errors' : '' }}>
		<label for="permission" class="col-md-2 control-label">Permission</label>
		<div class="col-md-4" style="overflow: scroll; height: 250px">
			@foreach($permission  as $value)
				<label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, ['class' => 'name']) }} {{ $value->name }}</label><br/>
			@endforeach
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-md-offset-2">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
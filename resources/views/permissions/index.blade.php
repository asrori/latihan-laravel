@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                    <li class="active">Permissions</li>
                </ul>
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">Permission</h3>
                    </div>

                    <div class="panel-body">
                        @role('admin')
                        <p><a class="btn btn-warning" href="{{ url('/admin/permission/create') }}"> Tambah Permission</a></p>
                        @endrole
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Display Name</th>
                                        <th>Created</th>
                                        <th>Updated</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($permissions as $key => $permission)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>{{ $permission->display_name }}</td>
                                        <td>{{ date('M d, Y', strtotime($permission->created_at)) }}</td>
                                        <td>{{ date('M d, Y', strtotime($permission->updated_at)) }}</td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ url('admin/permission/edit', $permission->id) }}">Edit</a>
                                        
                                            {!! Form::open(['method' => 'DELETE', 'url' => ['admin/permission/delete', $permission->id], 'style' => 'display:inline', 'onclick' => 'return confirm("Anda yakin akan menghapus data ?")']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                            {!! Form::close() !!}
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $permissions->render() !!}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
	<div class="form-group" {{ $errors->has('name') ? 'has-errors' : '' }}>
		<label for="name" class="col-md-2 control-label">Name Permission</label>
		<div class="col-md-4">
			{!! Form::text('name', null, ['placeholder' => 'Isi name dengan format camelCase', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('display') ? 'has-errors' : '' }}>
		<label for="display" class="col-md-2 control-label">Display Name</label>
		<div class="col-md-4">
			{!! Form::text('display_name', null, ['placeholder' => 'Display Name', 'class' => 'form-control']) !!}
		</div>
	</div>

	<div class="form-group" {{ $errors->has('description') ? 'has-errors' : '' }}>
		<label for="description" class="col-md-2 control-label">Description</label>
		<div class="col-md-4">
			 {!! Form::textarea('description', null, ['placeholder' => 'Description', 'class' => 'form-control', 'style' => 'height:100px']) !!}
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-4 col-md-offset-2">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
		</div>
	</div>
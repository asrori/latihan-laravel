@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{{ url('/home') }}">Dashboard</a></li>
					<li><a href="{{ url('/admin/permission') }}">Permissions</a></li>
					<li class="active">Create Permissions</a></li>
				</ul>
		
				<div class="panel panel-default">
					<div class="panel-heading">
						<h2 class="panel-title">Create Permission</h2>
					</div>
					<div class="panel-body">
						{!! Form::open(['url' => 'admin/permission/store', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
							@include('permissions._form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection('content')